# 更新日志

## 2023/5/22

支持LeNet、ResNet50、Vision Transformer训练，数据集包括
Fashion-MNIST、MNIST、CIRAF-10、CIRAF-100

数据集下载地址：

Fashion-MNIST：https://github.com/zalandoresearch/fashion-mnist#get-the-data

MNIST：http://yann.lecun.com/exdb/mnist/

CIFAR-10：https://www.cs.toronto.edu/~kriz/cifar.html

CIFAR-100：https://www.cs.toronto.edu/~kriz/cifar.html

下载完成后解压到 `./dataset` 的对应目录下

