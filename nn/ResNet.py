import torch.nn as nn

# 定义Bottleneck块
class Bottleneck(nn.Module):
    expansion = 4  # 扩展系数，用于计算输出通道数

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)  # 第一个1x1卷积层
        self.bn1 = nn.BatchNorm2d(planes)  # 第一个BatchNorm层
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)  # 第二个3x3卷积层
        self.bn2 = nn.BatchNorm2d(planes)  # 第二个BatchNorm层
        self.conv3 = nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False)  # 第三个1x1卷积层
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)  # 第三个BatchNorm层
        self.relu = nn.ReLU(inplace=True)  # ReLU激活函数
        self.downsample = downsample  # 跨层连接
        self.stride = stride  # 步长

    def forward(self, x):
        identity = x  # 保存输入

        out = self.conv1(x)  # 第一个1x1卷积层
        out = self.bn1(out)  # 第一个BatchNorm层
        out = self.relu(out)  # ReLU激活函数

        out = self.conv2(out)  # 第二个3x3卷积层
        out = self.bn2(out)  # 第二个BatchNorm层
        out = self.relu(out)  # ReLU激活函数

        out = self.conv3(out)  # 第三个1x1卷积层
        out = self.bn3(out)  # 第三个BatchNorm层

        if self.downsample is not None:  # 如果存在跨层连接
            identity = self.downsample(x)  # 跨层连接，将输入x通过downsample连接到out

        out += identity  # 加上跨层连接的输入identity
        out = self.relu(out)  # ReLU激活函数

        return out

# 定义ResNet50模型
class ResNet50(nn.Module):

    def __init__(self, class_number, in_channels):
        super(ResNet50, self).__init__()

        # 第一层卷积层
        self.inplanes = 64
        self.conv1 = nn.Conv2d(in_channels, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        # 四个残差块
        self.layer1 = self._make_layer(Bottleneck, 64, 3)  # 3个Bottleneck块，输入通道数为64
        self.layer2 = self._make_layer(Bottleneck, 128, 4, stride=2)  # 4个Bottleneck块，输入通道数为128，步长为2
        self.layer3 = self._make_layer(Bottleneck, 256, 6, stride=2)  # 6个Bottleneck块，输入通道数为256，步长为2
        self.layer4 = self._make_layer(Bottleneck, 512, 3, stride=2)  # 3个Bottleneck块，输入通道数为512，步长为2

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))  # 自适应平均池化层
        self.fc = nn.Linear(512 * Bottleneck.expansion, class_number)  # 全连接层

        # 参数初始化，使用kaiming_normal_和constant_函数
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:  # 如果输入输出通道数不一致，或者步长不为1，需要使用跨层连接
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []  # 构建残差块
        layers.append(block(self.inplanes, planes, stride, downsample))  # 第一个Bottleneck块
        self.inplanes = planes * block.expansion  # 更新输入通道数
        for _ in range(1, blocks):  # 其余Bottleneck块
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)  # 返回Bottleneck块的序列

    def forward(self, x):
        x = self.conv1(x)  # 第一层卷积层
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)  # 四个残差块
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)  # 自适应平均池化层
        x = x.view(x.size(0), -1)
        x = self.fc(x)  # 全连接层

        return x