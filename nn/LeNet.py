import torch.nn as nn

class LeNet_1C(nn.Module):
    def __init__(self,class_number, in_channels):
        super(LeNet_1C, self).__init__()
        
        # 第一层卷积层，输入通道数为1，输出通道数为6，卷积核大小为5
        self.conv1 = nn.Conv2d(in_channels, out_channels=6, kernel_size=5)
        
        # 第二层卷积层，输入通道数为6，输出通道数为16，卷积核大小为5
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5)
        
        # 池化层，池化核大小为2，步长为2
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        
        # 第一层全连接层，输入特征数为16 * 4 * 4，输出特征数为120
        self.fc1 = nn.Linear(in_features=16 * 4 * 4, out_features=120)
        
        # 第二层全连接层，输入特征数为120，输出特征数为84
        self.fc2 = nn.Linear(in_features=120, out_features=84)
        
        # 第三层全连接层，输入特征数为84，输出特征数为10
        self.fc3 = nn.Linear(in_features=84, out_features=class_number)
        
        # 激活函数ReLU
        self.relu = nn.ReLU()
        
    def forward(self, x):
        # 第一层卷积层，输入x，经过ReLU激活函数后再经过池化层
        x = self.pool(self.relu(self.conv1(x)))
        
        # 第二层卷积层，输入x，经过ReLU激活函数后再经过池化层
        x = self.pool(self.relu(self.conv2(x)))
        
        # 将x展开成一维向量
        b , c , w, h = x.shape

        x = x.view(b, c*w*h)
        
        # 第一层全连接层，输入x，经过ReLU激活函数
        x = self.relu(self.fc1(x))
        
        # 第二层全连接层，输入x，经过ReLU激活函数
        x = self.relu(self.fc2(x))
        
        # 第三层全连接层，输入x，不经过激活函数
        x = self.fc3(x)
        
        # 返回x
        return x

class LeNet_3C(nn.Module):
    def __init__(self, class_number, in_channels):
        super(LeNet_3C, self).__init__()
        
        # 第一层卷积层，输入通道数为3，输出通道数为6，卷积核大小为5
        self.conv1 = nn.Conv2d(in_channels, out_channels=6, kernel_size=5)
        
        # 第二层卷积层，输入通道数为6，输出通道数为16，卷积核大小为5
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5)
        
        # 池化层，池化核大小为2，步长为2
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        
        # 第一层全连接层，输入特征数为16 * 4 * 4，输出特征数为120
        self.fc1 = nn.Linear(in_features=16 * 5 * 5, out_features=120)
        
        # 第二层全连接层，输入特征数为120，输出特征数为84
        self.fc2 = nn.Linear(in_features=120, out_features=84)
        
        # 第三层全连接层，输入特征数为84，输出特征数为10
        self.fc3 = nn.Linear(in_features=84, out_features = class_number)
        
        # 激活函数ReLU
        self.relu = nn.ReLU()
        
    def forward(self, x):
        # 第一层卷积层，输入x，经过ReLU激活函数后再经过池化层
        x = self.pool(self.relu(self.conv1(x)))
        
        # 第二层卷积层，输入x，经过ReLU激活函数后再经过池化层
        x = self.pool(self.relu(self.conv2(x)))
        
        b , c , w, h = x.shape
        
        # 展开x
        x = x.view(b, c*w*h)
        
        # 第一层全连接层，输入x，经过ReLU激活函数
        x = self.relu(self.fc1(x))
        
        # 第二层全连接层，输入x，经过ReLU激活函数
        x = self.relu(self.fc2(x))
        
        # 第三层全连接层，输入x，不经过激活函数
        x = self.fc3(x)
        
        # 返回x
        return x