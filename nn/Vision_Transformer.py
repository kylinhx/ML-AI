import torch
from torch import nn

from einops import rearrange, repeat
from einops.layers.torch import Rearrange

# https://arxiv.org/abs/2010.11929
# https://github.com/lucidrains/vit-pytorch/blob/main/vit_pytorch/vit.py

# helpers
def pair(t):
    return t if isinstance(t, tuple) else (t, t)

# classes
class PreNorm(nn.Module):
    def __init__(self, dim, fn):
        super().__init__()
        self.norm = nn.LayerNorm(dim)  # 应用LayerNorm（规范化）到输入向量
        self.fn = fn  # 待应用的函数
    def forward(self, x, **kwargs):
        return self.fn(self.norm(x), **kwargs)  # 应用函数到规范化后的向量

class FeedForward(nn.Module):
    def __init__(self, dim, hidden_dim, dropout = 0.):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(dim, hidden_dim),  # 全连接层
            nn.GELU(),  # GELU激活函数
            nn.Dropout(dropout),  # Dropout层
            nn.Linear(hidden_dim, dim),  # 全连接层
            nn.Dropout(dropout)  # Dropout层
        )
    def forward(self, x):
        return self.net(x)  # 应用FeedForward网络

class Attention(nn.Module):
    def __init__(self, dim, heads = 8, dim_head = 64, dropout = 0.):
        super().__init__()
        inner_dim = dim_head *  heads
        project_out = not (heads == 1 and dim_head == dim)

        self.heads = heads  # 头数
        self.scale = dim_head ** -0.5  # 缩放因子

        self.attend = nn.Softmax(dim = -1)  # 注意力分数计算
        self.dropout = nn.Dropout(dropout)  # Dropout层

        self.to_qkv = nn.Linear(dim, inner_dim * 3, bias = False)  # 用于将输入向量映射到QKV三元组的全连接层

        self.to_out = nn.Sequential(
            nn.Linear(inner_dim, dim),  # 全连接层
            nn.Dropout(dropout)  # Dropout层
        ) if project_out else nn.Identity()  # 如果需要将输出投影到原始向量空间，则使用全连接层，否则使用恒等函数

    def forward(self, x):
        qkv = self.to_qkv(x).chunk(3, dim = -1)  # 将输入向量映射到QKV三元组
        q, k, v = map(lambda t: rearrange(t, 'b n (h d) -> b h n d', h = self.heads), qkv)  # 重组QKV张量

        dots = torch.matmul(q, k.transpose(-1, -2)) * self.scale  # 计算点积并缩放

        attn = self.attend(dots)  # 计算注意力分数
        attn = self.dropout(attn)  # 应用Dropout

        out = torch.matmul(attn, v)  # 计算加权平均值
        out = rearrange(out, 'b h n d -> b n (h d)')  # 重组输出张量
        return self.to_out(out)  # 投影输出向量

class Transformer(nn.Module):
    def __init__(self, dim, depth, heads, dim_head, mlp_dim, dropout = 0.):
        super().__init__()
        self.layers = nn.ModuleList([])
        for _ in range(depth):
            self.layers.append(nn.ModuleList([
                PreNorm(dim, Attention(dim, heads = heads, dim_head = dim_head, dropout = dropout)),  # 规范化 + 注意力层
                PreNorm(dim, FeedForward(dim, mlp_dim, dropout = dropout))  # 规范化 + FeedForward层
            ]))
    def forward(self, x):
        for attn, ff in self.layers:
            x = attn(x) + x  # 应用规范化 + 注意力层，并添加残差连接
            x = ff(x) + x  # 应用规范化 + FeedForward层，并添加残差连接
        return x

class VIT(nn.Module):
    def __init__(self, *, image_size, patch_size, num_classes, dim, depth, heads, mlp_dim, pool = 'cls', channels = 3, dim_head = 64, dropout = 0., emb_dropout = 0.):
        super().__init__()
        image_height, image_width = pair(image_size)
        patch_height, patch_width = pair(patch_size)

        assert image_height % patch_height == 0 and image_width % patch_width == 0, 'Image dimensions must be divisible by the patch size.'

        num_patches = (image_height // patch_height) * (image_width // patch_width)
        patch_dim = channels * patch_height * patch_width
        assert pool in {'cls', 'mean'}, 'pool type must be either cls (cls token) or mean (mean pooling)'

        self.to_patch_embedding = nn.Sequential(
            Rearrange('b c (h p1) (w p2) -> b (h w) (p1 p2 c)', p1 = patch_height, p2 = patch_width),  # 重组图像块
            nn.LayerNorm(patch_dim),  # 应用LayerNorm
            nn.Linear(patch_dim, dim),  # 全连接层
            nn.LayerNorm(dim),  # 应用LayerNorm
        )

        self.pos_embedding = nn.Parameter(torch.randn(1, num_patches + 1, dim))  # 位置编码
        self.cls_token = nn.Parameter(torch.randn(1, 1, dim))  # 分类令牌
        self.dropout = nn.Dropout(emb_dropout)  # Dropout层

        self.transformer = Transformer(dim, depth, heads, dim_head, mlp_dim, dropout)  # Transformer编码器

        self.pool = pool  # 池化类型
        self.to_latent = nn.Identity()  # 恒等函数

        self.mlp_head = nn.Sequential(
            nn.LayerNorm(dim),  # 应用LayerNorm
            nn.Linear(dim, num_classes)  # 全连接层
        )

    def forward(self, img):
        x = self.to_patch_embedding(img)  # 将输入图像转换为图像块
        b, n, _ = x.shape

        cls_tokens = repeat(self.cls_token, '1 1 d -> b 1 d', b = b)  # 重复分类令牌
        x = torch.cat((cls_tokens, x), dim=1)  # 将分类令牌添加到输入张量中
        x += self.pos_embedding[:, :(n + 1)]  # 添加位置编码
        x = self.dropout(x)  # 应用Dropout

        x = self.transformer(x)  # 应用Transformer编码器

        x = x.mean(dim = 1) if self.pool == 'mean' else x[:, 0]  # 池化

        x = self.to_latent(x)  # 应用恒等函数
        return self.mlp_head(x)  # 应用MLP头进行类别预测